# frozen_string_literal: true

namespace :deploy do
  after 'deploy:updated', 'nobrainer:create_nobrainer_indexes'
end

namespace :nobrainer do
  desc 'Runs rails nobrainer:sync_indexes'
  task :create_nobrainer_indexes do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rails, 'nobrainer:sync_indexes'
        end
      end
    end
  end

  desc 'Runs rails nobrainer:migrate'
  task :migrate_database do
    on fetch(:migration_servers) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rails, 'nobrainer:migrate'
        end
      end
    end
  end

  before 'nobrainer:create_nobrainer_indexes', 'nobrainer:migrate_database'
end

namespace :load do
  task :defaults do
    set :migration_role, fetch(:migration_role, :db)
    set :migration_servers, -> { primary(fetch(:migration_role)) }
  end
end
