# frozen_string_literal: true

module Capistrano
  module Nobrainer
    VERSION = '0.3.0'
  end
end
