# frozen_string_literal: true

require 'capistrano/nobrainer/version'

module Capistrano
  module Nobrainer
  end
end

load File.expand_path('tasks/nobrainer.rake', __dir__)
