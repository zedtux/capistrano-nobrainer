# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]



## [0.3.0] - 2020-11-02
### Added
- Executing `nobrainer:migrate` when `db/migrate` folder is present

## [0.2.0] - 2020-10-23
### Fixed
- Fixes allowed_push_host from the gemspec file

## [0.1.0] - 2020-10-23
### Added
- Capistrano task to create Nobrainer indexes

[Unreleased]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v0.1.0...master
[0.1.0]: https://gitlab.com/zedtux/changelog-notifier/-/tags/v0.1.0
