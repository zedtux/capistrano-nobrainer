# Capistrano::Nobrainer

This gem adds the integration for the nobrainer gem in Capistrano.

It creates the indexes automatically and run the database migration if any (WARNING: You need to use the zedtux's nobrainer fork in order to use migration scripts until it get merged).

| Feature                     | capistrano-nobrainer version  |
| --------------------------- | ----------------------------- |
| Nobrainer indexes creation  | 0.2.0                         |
| Nobrainer migration scripts | 0.3.0                         |

So if you don't use migration scripts, stick on version 0.2.0.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'capistrano-nobrainer', '~> 0.2', require: false
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install capistrano-nobrainer

## Usage

Add the following require to your `Capfile`:

```ruby
require 'capistrano/nobrainer'
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/zedtux/capistrano-nobrainer. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/zedtux/capistrano-nobrainer/blob/master/CODE_OF_CONDUCT.md).


## Code of Conduct

Everyone interacting in the Capistrano::Nobrainer project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/zedtux/capistrano-nobrainer/blob/master/CODE_OF_CONDUCT.md).
