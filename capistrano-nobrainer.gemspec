# frozen_string_literal: true

require_relative 'lib/capistrano/nobrainer/version'

repo_url = 'https://gitlab.com/zedtux/capistrano-nobrainer'

Gem::Specification.new do |spec|
  spec.name          = 'capistrano-nobrainer'
  spec.version       = Capistrano::Nobrainer::VERSION
  spec.authors       = ['Guillaume Hain']
  spec.email         = ['zedtux@zedroot.org']

  spec.summary       = 'Nobrainer integration for Capistrano'
  spec.description   = File.read('README.md').scan(
    /\A#.*$\n\n([\w\W]+)\n\n##/
  ).flatten

  spec.homepage = repo_url
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = repo_url
  spec.metadata['changelog_uri'] = repo_url + '/-/blob/master/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added
  # into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
